/*
 * OHCI HCD (Host Controller Driver) for USB.
 *
 * (C) Copyright 1999 Roman Weissgaerber <weissg@vienna.at>
 * (C) Copyright 2000-2005 David Brownell
 * (C) Copyright 2002 Hewlett-Packard Company
 *
 * OMAP Bus Glue
 *
 * Modified for OMAP by Tony Lindgren <tony@atomide.com>
 * Based on the 2.4 OMAP OHCI driver originally done by MontaVista Software Inc.
 * and on ohci-sa1111.c by Christopher Hoover <ch@hpl.hp.com>
 *
 * This file is licenced under the GPL.
 */

#include <linux/signal.h>	/* IRQF_DISABLED */
#include <linux/jiffies.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <linux/gpio.h>

#include <mach/hardware.h>
#include <asm/io.h>
#include <asm/mach-types.h>

#include <mach/mux.h>
#include <mach/irqs.h>
#include <mach/fpga.h>
#include <mach/usb.h>

#include "ehci-omap.h" //get some headers, copy over sometime

#define DEBUG_BEN	//printk("%s: %d\n", __FILE__, __LINE__);



#ifndef CONFIG_ARCH_OMAP3
#error "This file is OMAP3 bus glue.  CONFIG_OMAP must be defined."
#endif

#define LOW		0
#define HIGH	1


extern int usb_disabled(void);
extern int ocpi_enable(void);

/*-------------------------------------------------------------------------*/
/* Define USBHOST clocks for clock management */
static struct clk	*usbhost_ick_clk;
static struct clk	*usbhost2_120m_fck_clk;
static struct clk	*usbhost1_48m_fck_clk;
static struct clk	*usbtll_fck_clk;
static struct clk	*usbtll_ick_clk;

/* Clock names as per clock framework: May change so keep as #defs */
#define USBHOST_ICKL		"usbhost_ick"
#define USBHOST_120M_FCLK	"usbhost_120m_fck"
#define USBHOST_48M_FCLK	"usbhost_48m_fck"
#define USBHOST_TLL_ICKL	"usbtll_ick"
#define USBHOST_TLL_FCLK	"usbtll_fck"
/*-------------------------------------------------------------------------*/

static int host_enabled;
static int host_initialized;

static void omap3_ohci_clock_power(int on)
{

	if (on) {
		clk_enable(usbhost_ick_clk);
		clk_enable(usbhost2_120m_fck_clk);
		clk_enable(usbhost1_48m_fck_clk);
		clk_enable(usbtll_fck_clk);
		clk_enable(usbtll_ick_clk);
		udelay(100);
	} else {
		clk_disable(usbhost_ick_clk);
		clk_disable(usbhost2_120m_fck_clk);
		clk_disable(usbhost1_48m_fck_clk);
		clk_disable(usbtll_fck_clk);
		clk_disable(usbtll_ick_clk);
	}

}

/*
 * Board specific gang-switched transceiver power on/off.
 */
static int omap3_ohci_transceiver_power(int on)
{
	if (on) {
		;

	} else {
		;

	}

	return 0;
}


#ifdef	CONFIG_USB_OTG

static void start_hnp(struct ohci_hcd *ohci)
{
	const unsigned	port = ohci_to_hcd(ohci)->self.otg_port - 1;
	unsigned long	flags;
	u32 l;

	otg_start_hnp(ohci->transceiver);

	local_irq_save(flags);
	ohci->transceiver->state = OTG_STATE_A_SUSPEND;
	writel (RH_PS_PSS, &ohci->regs->roothub.portstatus [port]);
	l = omap_readl(OTG_CTRL);
	l &= ~OTG_A_BUSREQ;
	omap_writel(l, OTG_CTRL);
	local_irq_restore(flags);
}

#endif

/*-------------------------------------------------------------------------*/

static int ohci_omap3_init(struct usb_hcd *hcd)
{
	struct ohci_hcd		*ohci = hcd_to_ohci(hcd);
	struct omap_usb_config	*config = hcd->self.controller->platform_data;
	int			need_transceiver = (config->otg != 0);
	int			ret;

	dev_dbg(hcd->self.controller, "starting USB Controller\n");
	DEBUG_BEN;
	
	if (config->otg) {
		DEBUG_BEN;
		ohci_to_hcd(ohci)->self.otg_port = config->otg;
		/* default/minimum OTG power budget:  8 mA */
		DEBUG_BEN;
		ohci_to_hcd(ohci)->power_budget = 8;
	}
	DEBUG_BEN;
	/* boards can use OTG transceivers in non-OTG modes */
	need_transceiver = need_transceiver
			|| machine_is_omap_h2() || machine_is_omap_h3();

	if (cpu_is_omap16xx())
		ocpi_enable();
	DEBUG_BEN;
#ifdef	CONFIG_USB_OTG
	if (need_transceiver) {
		ohci->transceiver = otg_get_transceiver();
		if (ohci->transceiver) {
			int	status = otg_set_host(ohci->transceiver,
						&ohci_to_hcd(ohci)->self);
			dev_dbg(hcd->self.controller, "init %s transceiver, status %d\n",
					ohci->transceiver->label, status);
			if (status) {
				if (ohci->transceiver)
					put_device(ohci->transceiver->dev);
				return status;
			}
		} else {
			dev_err(hcd->self.controller, "can't find transceiver\n");
			return -ENODEV;
		}
		ohci->start_hnp = start_hnp;
	}
#endif
	DEBUG_BEN;
	omap3_ohci_clock_power(1);


	DEBUG_BEN;
	if ((ret = ohci_init(ohci)) < 0)
		return ret;


	DEBUG_BEN;
	/* FIXME khubd hub requests should manage power switching */
	omap3_ohci_transceiver_power(1);

	/* board init will have already handled HMC and mux setup.
	 * any external transceiver should already be initialized
	 * too, so all configured ports use the right signaling now.
	 */
	DEBUG_BEN;
	return 0;
}

static void ohci_omap3_stop(struct usb_hcd *hcd)
{
	dev_dbg(hcd->self.controller, "stopping USB Controller\n");
	omap3_ohci_clock_power(0);
}

/*
 * omap3_oswald_start_usb_bus_setup
 * Sets up the usb bus stuff for the oswald board
 */
static int omap3_oswald_start_usb_bus_setup(struct platform_device *dev) {
	
	
	/* Start DPLL5 Programming:
	 * Clock Framework is not doing this now:
	 * This will be done in clock framework later
	 */
	cm_write_mod_reg((12 << OMAP3430ES2_PERIPH2_DPLL_DIV_SHIFT)|
			(120 << OMAP3430ES2_PERIPH2_DPLL_MULT_SHIFT),
			PLL_MOD, OMAP3430ES2_CM_CLKSEL4);

	cm_write_mod_reg(1 << OMAP3430ES2_DIV_120M_SHIFT,
			PLL_MOD, OMAP3430ES2_CM_CLKSEL5);

	cm_write_mod_reg((7 << OMAP3430ES2_PERIPH2_DPLL_FREQSEL_SHIFT) |
			(7 << OMAP3430ES2_EN_PERIPH2_DPLL_SHIFT),
			PLL_MOD, OMAP3430ES2_CM_CLKEN2);

	while (!(cm_read_mod_reg(PLL_MOD, CM_IDLEST2) &
			OMAP3430ES2_ST_PERIPH2_CLK_MASK))
		cpu_relax();
	/* End DPLL5 programming */
	
		
	/* PRCM settings for USBHOST:
	 * Interface clk un-related to domain transition
	 */
	cm_write_mod_reg(0 << OMAP3430ES2_AUTO_USBHOST_SHIFT,
				OMAP3430ES2_USBHOST_MOD, CM_AUTOIDLE);

	/* Disable sleep dependency with MPU and IVA */
	cm_write_mod_reg((0 << OMAP3430ES2_EN_MPU_SHIFT) |
				(0 << OMAP3430ES2_EN_IVA2_SHIFT),
				OMAP3430ES2_USBHOST_MOD, OMAP3430_CM_SLEEPDEP);

	/* Disable Automatic transition of clock */
	cm_write_mod_reg(0 << OMAP3430ES2_CLKTRCTRL_USBHOST_SHIFT,
				OMAP3430ES2_USBHOST_MOD, CM_CLKSTCTRL);


	DEBUG_BEN

	/* Enable Clocks for USBHOST */
	usbhost_ick_clk = clk_get(&dev->dev, USBHOST_ICKL);
	if (IS_ERR(usbhost_ick_clk))
		return PTR_ERR(usbhost_ick_clk);
	clk_enable(usbhost_ick_clk);

	DEBUG_BEN

	usbhost2_120m_fck_clk = clk_get(&dev->dev, USBHOST_120M_FCLK);
	if (IS_ERR(usbhost2_120m_fck_clk))
		return PTR_ERR(usbhost2_120m_fck_clk);
	clk_enable(usbhost2_120m_fck_clk);

	DEBUG_BEN

	usbhost1_48m_fck_clk = clk_get(&dev->dev, USBHOST_48M_FCLK);
	if (IS_ERR(usbhost1_48m_fck_clk))
		return PTR_ERR(usbhost1_48m_fck_clk);
	clk_enable(usbhost1_48m_fck_clk);

	DEBUG_BEN
	
	usbtll_fck_clk = clk_get(&dev->dev, USBHOST_TLL_FCLK);
	if (IS_ERR(usbtll_fck_clk))
		return PTR_ERR(usbtll_fck_clk);
	clk_enable(usbtll_fck_clk);

	DEBUG_BEN

	usbtll_ick_clk = clk_get(&dev->dev, USBHOST_TLL_ICKL);
	if (IS_ERR(usbtll_ick_clk))
		return PTR_ERR(usbtll_ick_clk);
	clk_enable(usbtll_ick_clk);

	udelay(100); //Just for good measure...
	
	DEBUG_BEN
	
	/* Disable Auto Idle of USBTLL */
	cm_write_mod_reg((0 << OMAP3430ES2_AUTO_USBTLL_SHIFT),
				CORE_MOD, CM_AUTOIDLE3);
				
	DEBUG_BEN

	/* perform TLL soft reset, and wait until reset is complete */
	omap_writel(1 << OMAP_USBTLL_SYSCONFIG_SOFTRESET_SHIFT,
			OMAP_USBTLL_SYSCONFIG);

	DEBUG_BEN

	/* Wait for TLL reset to complete */					//FIXME: why does this break???
	while (!(omap_readl(OMAP_USBTLL_SYSSTATUS)
			& (1 << OMAP_USBTLL_SYSSTATUS_RESETDONE_SHIFT)))
		cpu_relax();
		
		
	DEBUG_BEN
	
	/* (1<<3) = no idle mode only for initial debugging */
	omap_writel((1 << OMAP_USBTLL_SYSCONFIG_ENAWAKEUP_SHIFT) |
			(1 << OMAP_USBTLL_SYSCONFIG_SIDLEMODE_SHIFT) |
			(1 << OMAP_USBTLL_SYSCONFIG_CACTIVITY_SHIFT),
			OMAP_USBTLL_SYSCONFIG);

	DEBUG_BEN;
	/* Put UHH in NoIdle/NoStandby mode */
	omap_writel((0 << OMAP_UHH_SYSCONFIG_AUTOIDLE_SHIFT) |
			(1 << OMAP_UHH_SYSCONFIG_ENAWAKEUP_SHIFT) |
			(1 << OMAP_UHH_SYSCONFIG_SIDLEMODE_SHIFT) |
			(1 << OMAP_UHH_SYSCONFIG_CACTIVITY_SHIFT) |
			(1 << OMAP_UHH_SYSCONFIG_MIDLEMODE_SHIFT),
			OMAP_UHH_SYSCONFIG);
			
	DEBUG_BEN;
	omap_writel(0x00000001, 0x48064040); //switch to correct usb mode
	omap_writel(0x01018003, 0x48062040); //channel 0
	omap_writel(0x01018003, 0x48062044); //channel 1
	omap_writel(0x01018003, 0x48062048); //channel 2
	
	DEBUG_BEN;
	
	/* Enusre bit is set */
	while (!(omap_readl(OMAP_UHH_HOSTCONFIG)
			& (1 << OMAP_UHH_HOSTCONFIG_ULPI_BYPASS_SHIFT)))
		cpu_relax();

	DEBUG_BEN;
	
	return 0;
}


/*-------------------------------------------------------------------------*/

/**
 * usb_hcd_omap_probe - initialize OMAP-based HCDs
 * Context: !in_interrupt()
 *
 * Allocates basic resources for this USB host controller, and
 * then invokes the start() method for the HCD associated with it
 * through the hotplug entry's driver_data.
 */
static int usb_hcd_omap3_probe (const struct hc_driver *driver,
			  struct platform_device *pdev)
{
	int retval, irq;
	struct usb_hcd *hcd = 0;
	struct ohci_hcd *ohci;
	
	printk("ohci-omap3: probing\n");

	if (pdev->num_resources != 2) {
		printk(KERN_ERR "hcd probe: invalid num_resources: %i\n",
		       pdev->num_resources);
		return -ENODEV;
	}

	if (pdev->resource[0].flags != IORESOURCE_MEM
			|| pdev->resource[1].flags != IORESOURCE_IRQ) {
		printk(KERN_ERR "hcd probe: invalid resource type\n");
		return -ENODEV;
	}
	
	

	hcd = usb_create_hcd (driver, &pdev->dev, dev_name(&pdev->dev));
	if (!hcd) {
		retval = -ENOMEM;
		goto err0;
	}
	
	DEBUG_BEN
	
	omap3_oswald_start_usb_bus_setup(pdev);
	
	hcd->rsrc_start = pdev->resource[0].start;
	hcd->rsrc_len = pdev->resource[0].end - pdev->resource[0].start + 1;

	DEBUG_BEN

	if (!request_mem_region(hcd->rsrc_start, hcd->rsrc_len, hcd_name)) {
		dev_dbg(&pdev->dev, "request_mem_region failed\n");
		retval = -EBUSY;
		goto err1;
	}
	
	DEBUG_BEN

	hcd->regs = ioremap(hcd->rsrc_start, hcd->rsrc_len);
	if (!hcd->regs) {
		dev_err(&pdev->dev, "can't ioremap OHCI HCD\n");
		retval = -ENOMEM;
		goto err2;
	}

	DEBUG_BEN

	ohci = hcd_to_ohci(hcd);
	ohci_hcd_init(ohci);

	host_initialized = 0;
	host_enabled = 1;

	DEBUG_BEN

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		retval = -ENXIO;
		goto err3;
	}
	retval = usb_add_hcd(hcd, irq, IRQF_DISABLED);
	if (retval)
		goto err3;

	host_initialized = 1;

	DEBUG_BEN

	if (!host_enabled)
//		omap3_ohci_clock_power(0);

	return 0;
err3:
	iounmap(hcd->regs);
err2:
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
err1:
	usb_put_hcd(hcd);
err0:
	return retval;
}


/* may be called with controller, bus, and devices active */

/**
 * usb_hcd_omap_remove - shutdown processing for OMAP-based HCDs
 * @dev: USB Host Controller being removed
 * Context: !in_interrupt()
 *
 * Reverses the effect of usb_hcd_omap_probe(), first invoking
 * the HCD's stop() method.  It is always called from a thread
 * context, normally "rmmod", "apmd", or something similar.
 */
 //TODO: properly stop the clocks 
static inline void
usb_hcd_omap3_remove (struct usb_hcd *hcd, struct platform_device *pdev)
{
	struct ohci_hcd		*ohci = hcd_to_ohci (hcd);

	usb_remove_hcd(hcd);
	if (ohci->transceiver) {
		(void) otg_set_host(ohci->transceiver, 0);
		put_device(ohci->transceiver->dev);
	}
	if (machine_is_omap_osk())
		gpio_free(9);
	iounmap(hcd->regs);
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
	usb_put_hcd(hcd);

}

/*-------------------------------------------------------------------------*/

static int
ohci_omap3_start (struct usb_hcd *hcd)
{
	struct omap_usb_config *config;
	struct ohci_hcd	*ohci = hcd_to_ohci (hcd);
	int		ret;

	if (!host_enabled)
		return 0;
	config = hcd->self.controller->platform_data;
	if (config->otg || config->rwc) {
		ohci->hc_control = OHCI_CTRL_RWC;
		writel(OHCI_CTRL_RWC, &ohci->regs->control);
	}

	if ((ret = ohci_run (ohci)) < 0) {
		dev_err(hcd->self.controller, "can't start\n");
		ohci_stop (hcd);
		return ret;
	}
	return 0;
}

/*-------------------------------------------------------------------------*/

static const struct hc_driver ohci_omap3_hc_driver = {
	.description =		hcd_name,
	.product_desc =		"OMAP3 OHCI",
	.hcd_priv_size =	sizeof(struct ohci_hcd),

	/*
	 * generic hardware linkage
	 */
	.irq =			ohci_irq,
	.flags =		HCD_USB11 | HCD_MEMORY,

	/*
	 * basic lifecycle operations
	 */
	.reset =		ohci_omap3_init,
	.start =		ohci_omap3_start,
	.stop =			ohci_omap3_stop,
	.shutdown =		ohci_shutdown,

	/*
	 * managing i/o requests and associated device resources
	 */
	.urb_enqueue =		ohci_urb_enqueue,
	.urb_dequeue =		ohci_urb_dequeue,
	.endpoint_disable =	ohci_endpoint_disable,

	/*
	 * scheduling support
	 */
	.get_frame_number =	ohci_get_frame,

	/*
	 * root hub support
	 */
	.hub_status_data =	ohci_hub_status_data,
	.hub_control =		ohci_hub_control,
#ifdef	CONFIG_PM
	.bus_suspend =		ohci_bus_suspend,
	.bus_resume =		ohci_bus_resume,
#endif
	.start_port_reset =	ohci_start_port_reset,
};

/*-------------------------------------------------------------------------*/

static int ohci_hcd_omap3_drv_probe(struct platform_device *dev)
{
	return usb_hcd_omap3_probe(&ohci_omap3_hc_driver, dev);
}

static int ohci_hcd_omap3_drv_remove(struct platform_device *dev)
{
	struct usb_hcd		*hcd = platform_get_drvdata(dev);

	usb_hcd_omap3_remove(hcd, dev);
	platform_set_drvdata(dev, NULL);

	return 0;
}

/*-------------------------------------------------------------------------*/

#ifdef	CONFIG_PM

static int ohci_omap3_suspend(struct platform_device *dev, pm_message_t message)
{
	struct ohci_hcd	*ohci = hcd_to_ohci(platform_get_drvdata(dev));

	if (time_before(jiffies, ohci->next_statechange))
		msleep(5);
	ohci->next_statechange = jiffies;

	omap3_ohci_clock_power(0);
	ohci_to_hcd(ohci)->state = HC_STATE_SUSPENDED;
	return 0;
}

static int ohci_omap3_resume(struct platform_device *dev)
{
	struct usb_hcd	*hcd = platform_get_drvdata(dev);
	struct ohci_hcd	*ohci = hcd_to_ohci(hcd);

	if (time_before(jiffies, ohci->next_statechange))
		msleep(5);
	ohci->next_statechange = jiffies;

	omap3_ohci_clock_power(1);
	ohci_finish_controller_resume(hcd);
	return 0;
}

#endif

/*-------------------------------------------------------------------------*/

/*
 * Driver definition to register with the OMAP bus
 */
static struct platform_driver ohci_hcd_omap3_driver = {
	.probe		= ohci_hcd_omap3_drv_probe,
	.remove		= ohci_hcd_omap3_drv_remove,
	.shutdown	= usb_hcd_platform_shutdown,
#ifdef	CONFIG_PM
	.suspend	= ohci_omap3_suspend,
	.resume		= ohci_omap3_resume,
#endif
	.driver		= {
		.owner	= THIS_MODULE,
		.name	= "ohci-omap3",
	},
};

MODULE_ALIAS("platform:ohci");
