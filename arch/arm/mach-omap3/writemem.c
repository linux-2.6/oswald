#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/io.h>

int regval = 0;
int writeval = 0;

MODULE_LICENSE ("GPL");

module_param (regval,int,0);
MODULE_PARM_DESC (regval,"regval is an absolute address.");

module_param (writeval,int,0);
MODULE_PARM_DESC (writeval,"writeval is the integer to write to memory.");


static int writemem_init(void) {
	
	omap_writel(writeval,regval);
	return 0;
}

static void writemem_exit(void) {
//	printk("Readmem out\n");
}

module_init(writemem_init);
module_exit(writemem_exit);
