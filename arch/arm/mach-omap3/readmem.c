#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <asm/io.h>

int regval = 0;

MODULE_LICENSE ("GPL");
module_param (regval,int,0);
MODULE_PARM_DESC (regval,"regval is am absolute address.");


static int readmem_init(void) {
	
	printk("0x%08X\n", omap_readl(regval));
	return 0;
}

static void readmem_exit(void) {
//	printk("Readmem out\n");
}

module_init(readmem_init);
module_exit(readmem_exit);
