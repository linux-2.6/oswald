/*
 * linux/arch/arm/mach-omap2/usb-ohci.c
 *
 * This file will contain the board specific details for the
 * Synopsys OHCI host controller on OMAP3430
 *
 * Copyright (C) 2007 Texas Instruments
 * Author: Vikram Pandita <vikram.pandita@ti.com>
 *
 * Generalization by:
 * Felipe Balbi <felipe.balbi@nokia.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/types.h>
#include <linux/errno.h>
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/clk.h>
#include <asm/io.h>
#include <mach/mux.h>

#include <mach/hardware.h>
#include <mach/pm.h>
#include <mach/usb.h>

#if	defined(CONFIG_USB_OHCI_HCD) || defined(CONFIG_USB_OHCI_HCD_MODULE)

static struct omap_usb_config omap3_oswald_usb_config __initdata = {
	.pins[0]	= 6,
	.pins[1]	= 6,
	.pins[2]	= 6,
	.rwc		= 0,						// what does this flag?
	.hmc_mode	= 0x11,						// FIXME: how the hell is this used??
	.otg		= 0,
	.register_host = 1,
	.register_dev = 0,
};

static struct resource ohci_resources[] = {
	[0] = {
		.start   = OMAP34XX_HSUSB_HOST_BASE + 0x400,
		.end     = OMAP34XX_HSUSB_HOST_BASE + 0x400 + SZ_1K - 1,
		.flags   = IORESOURCE_MEM,
	},
	[1] = {         /* general IRQ */
		.start   = INT_34XX_OHCI_IRQ,
		.flags   = IORESOURCE_IRQ,
	}
};

static u64 ohci_dmamask = ~(u32)0;
static struct platform_device ohci_device = {
	.name           = "ohci-omap",
	.id             = 0,
	.dev = {
		.dma_mask               = &ohci_dmamask,
		.coherent_dma_mask      = 0xffffffff,
		.platform_data          = &omap3_oswald_usb_config,
	},
	.num_resources  = ARRAY_SIZE(ohci_resources),
	.resource       = ohci_resources,
};



#endif /* OHCI specific data */

void __init usb_ohci_init(void)
{
#if     defined(CONFIG_USB_OHCI_HCD) || defined(CONFIG_USB_OHCI_HCD_MODULE)
	printk(KERN_ERR "Attempting to register FS-USB (OHCI) device\n");
	if (platform_device_register(&ohci_device) < 0) {
		printk(KERN_ERR "Unable to register FS-USB (OHCI) device\n");
		return;
	}
#endif
}

