/*
 * MMC definitions for OMAP3 Oswald board
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

struct oswald_hsmmc_info {
	u8	mmc;		/* controller 1/2/3 */
	u8	wires;		/* 1/4/8 wires */
	int	gpio_cd;	/* or -EINVAL */
	int	gpio_wp;	/* or -EINVAL */
	int	ext_clock:1;	/* use external pin for input clock */
};

#ifdef CONFIG_MACH_OMAP3_OSWALD

void oswald_mmc_init(struct oswald_hsmmc_info *);

#else

static inline void oswald_mmc_init(struct oswald_hsmmc_info *info)
{
}

#endif
