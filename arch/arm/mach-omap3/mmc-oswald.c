/*
 * linux/arch/arm/mach-omap2/mmc-twl4030.c
 *
 * Copyright (C) 2007-2008 Texas Instruments
 * Copyright (C) 2008 Nokia Corporation
 * Author: Texas Instruments
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
#include <linux/err.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/gpio.h>

#include <mach/hardware.h>
#include <mach/control.h>
#include <mach/mmc.h>
#include <mach/board.h>

#include "mmc-oswald.h"

#ifdef CONFIG_MACH_OMAP3_OSWALD

#define LDO_CLR			0x00
#define VSEL_S2_CLR		0x40

#define VMMC1_DEV_GRP		0x27
#define VMMC1_CLR		0x00
#define VMMC1_315V		0x03
#define VMMC1_300V		0x02
#define VMMC1_285V		0x01
#define VMMC1_185V		0x00
#define VMMC1_DEDICATED		0x2A

#define VMMC2_DEV_GRP		0x2B
#define VMMC2_CLR		0x40
#define VMMC2_315V		0x0c
#define VMMC2_300V		0x0b
#define VMMC2_285V		0x0a
#define VMMC2_260V		0x08
#define VMMC2_185V		0x06
#define VMMC2_DEDICATED		0x2E

#define VMMC_DEV_GRP_P1		0x20

static u16 control_pbias_offset;
static u16 control_devconf1_offset;

#define HSMMC_NAME_LEN	9


static struct oswald_mmc_controller {
	struct	omap_mmc_platform_data	*mmc;
	char	name[HSMMC_NAME_LEN];
} hsmmc;

static int oswald_mmc_card_detect(int irq)
{
	struct omap_mmc_platform_data *mmc;
	//printk("oswald_mmc_card_detect\n");

	mmc = hsmmc.mmc;

	/* NOTE: assumes card detect signal is active-low */
	return !gpio_get_value_cansleep(mmc->slots[0].switch_pin);
}

static int oswald_mmc_get_ro(struct device *dev, int i)
{
	struct omap_mmc_platform_data *mmc = dev->platform_data;
	//printk("oswald_mmc_get_ro\n");
	
	/* NOTE: assumes write protect signal is active-high */
	return gpio_get_value_cansleep(mmc->slots[0].gpio_wp);
}

/*
 * MMC Slot Initialization.
 */
static int oswald_mmc_late_init(struct device *dev)
{
	struct omap_mmc_platform_data *mmc = dev->platform_data;
	int ret = 0;
	//printk("omap_mmc_late_init\n");
	ret = gpio_request(mmc->slots[0].switch_pin, "mmc_cd");
	if (ret)
		goto done;
		
	ret = gpio_direction_input(mmc->slots[0].switch_pin);
	
	if (ret)
		goto err;

	if (hsmmc.name == mmc->slots[0].name) {
		hsmmc.mmc = mmc;
	}


	return 0;

err:
	gpio_free(mmc->slots[0].switch_pin);
done:
	mmc->slots[0].card_detect_irq = 0;
	mmc->slots[0].card_detect = NULL;

	dev_err(dev, "err %d configuring card detect\n", ret);
	return ret;
}

static void oswald_mmc_cleanup(struct device *dev)
{
	struct omap_mmc_platform_data *mmc = dev->platform_data;
	//printk("oswald_mmc_cleanup\n");
	gpio_free(mmc->slots[0].switch_pin);
}

static int oswald_mmc_suspend(struct device *dev, int i)
{
	struct omap_mmc_platform_data *mmc = dev->platform_data;
	//printk("oswald_mmc_suspend\n");
	disable_irq(mmc->slots[0].card_detect_irq);
	return 0;
}

static int oswald_mmc_resume(struct device *dev, int i)
{
	struct omap_mmc_platform_data *mmc = dev->platform_data;
	//printk("oswald_mmc_resume\n");
	enable_irq(mmc->slots[0].card_detect_irq);
	return 0;
}

static int oswald_mmc_set_power(struct device *dev, int slot, int power_on, int vdd)
{
	return 1;
}


static struct omap_mmc_platform_data *hsmmc_data[OMAP34XX_NR_MMC] __initdata;

void __init oswald_mmc_init(struct oswald_hsmmc_info *c)
{
//	struct oswald_hsmmc_info *c;

	struct oswald_mmc_controller *oswald = &hsmmc + c->mmc - 1;
	struct omap_mmc_platform_data *mmc = hsmmc_data[c->mmc - 1];

	int nr_hsmmc = ARRAY_SIZE(hsmmc_data);
	
	//printk("Initing mmc for OMAP3 Oswald\n");
	
	control_pbias_offset = OMAP343X_CONTROL_PBIAS_LITE;
	control_devconf1_offset = OMAP343X_CONTROL_DEVCONF1;


	if (!c->mmc || c->mmc > nr_hsmmc) {
		pr_debug("MMC%d: no such controller\n", c->mmc);
		return;
	}
	
	if (mmc) {															// Probably not needed...
		pr_debug("MMC%d: already configured\n", c->mmc);
		return;
	}

	mmc = kzalloc(sizeof(struct omap_mmc_platform_data), GFP_KERNEL);
	if (!mmc) {
		pr_err("Cannot allocate memory for mmc device!\n");
		return;
	}

	sprintf(oswald->name, "mmc%islot%i", c->mmc, 1);
	mmc->slots[0].name = oswald->name;
	mmc->nr_slots = 1;
	mmc->slots[0].ocr_mask = MMC_VDD_165_195 |
				MMC_VDD_26_27 | MMC_VDD_27_28 |
				MMC_VDD_29_30 |
				MMC_VDD_30_31 | MMC_VDD_31_32;
	mmc->slots[0].wires = c->wires;
	mmc->slots[0].internal_clock = !c->ext_clock;
	mmc->dma_mask = 0xffffffff;


	printk("Trying to setup cd for mmc: %s\n", mmc->slots[0].name);
	if (gpio_is_valid(c->gpio_cd)) {
	
		mmc->init = oswald_mmc_late_init;
		mmc->cleanup = oswald_mmc_cleanup;
		mmc->suspend = oswald_mmc_suspend;
		mmc->resume = oswald_mmc_resume;


		gpio_request(c->gpio_cd, "mmc_cd");
		gpio_direction_input(c->gpio_cd);
		
		mmc->slots[0].switch_pin = c->gpio_cd;
		mmc->slots[0].card_detect_irq = gpio_to_irq(c->gpio_cd);
		mmc->slots[0].card_detect = oswald_mmc_card_detect;
	} 
	else { 
		printk("Invalid gpio for cd\n");
		mmc->slots[0].switch_pin = -EINVAL;
	}

	/* write protect normally uses an OMAP gpio */
	if (gpio_is_valid(c->gpio_wp)) {
	
		gpio_request(c->gpio_wp, "mmc_wp");
		gpio_direction_input(c->gpio_wp);

		mmc->slots[0].gpio_wp = c->gpio_wp;
		mmc->slots[0].get_ro = oswald_mmc_get_ro;
	} else
		mmc->slots[0].gpio_wp = -EINVAL;


	switch (c->mmc) {
	case 1:
		mmc->slots[0].set_power = oswald_mmc_set_power;
		break;
	default:
		pr_err("MMC%d configuration not supported!\n", c->mmc);
		return;
	}
	hsmmc_data[c->mmc - 1] = mmc;


	omap2_init_mmc(hsmmc_data, OMAP34XX_NR_MMC);					//FIXME: we want nothing to do with omap2 stuffs... we're an omap#3!
}

#endif
