/*
 * linux/arch/arm/mach-omap2/board-omap3oswald.c
 * Copyright (C) 2008 Texas Instruments
 *
 * Modified from mach-omap2/board-3430sdp.c
 *
 * Initial code: Syed Mohammed Khasim
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/clk.h>
#include <linux/spi/spi.h>
#include <linux/spi/spi_bitbang.h>
#include <linux/spi/ads7846.h>
#include <linux/io.h>
#include <linux/leds.h>
#include <linux/gpio.h>
#include <linux/input.h>
#include <linux/gpio_keys.h>

#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/nand.h>

#include <linux/regulator/machine.h>

#include <mach/hardware.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/flash.h>

#include <mach/board.h>
#include <mach/mcspi.h>
//#include <mach/usb-musb.h>
//#include <mach/usb-ohci.h> //both of them for now, fixme later
//#include <mach/usb-ehci.h>
#include <mach/usb.h>
#include <mach/common.h>
#include <mach/gpmc.h>
#include <mach/nand.h>
#include <mach/mux.h>
#include <mach/omapfb.h>
#include <mach/display.h>

#include "mmc-oswald.h"


//GPIO
#define POWER_SW_GPIO		1
#define TS_PEN_GPIO			133
#define TS_BSY_GPIO			151
#define V3_EN_GPIO			137
#define V5_EN_GPIO			18
#define BKLIGHT_EN_GPIO		55
#define CODEC_RST_GPIO		26
#define DVI_EN_GPIO			130
#define DVI_DETECT_GPIO		138
#define USER_LED_GPIO		19
#define MMC_LED_GPIO		13
#define MMC_WP_GPIO			136
#define MMC_CD_GPIO			139



#define GPMC_CS0_BASE	0x60
#define GPMC_CS_SIZE	0x30

#define NAND_BLOCK_SIZE	SZ_128K

////////////////////////////////////////////////////////////////////////////////
// NAND flash configuration...

static struct mtd_partition omap3oswald_nand_partitions[] = {
	/* All the partition sizes are listed in terms of NAND block size */
	{
		.name		= "X-Loader",
		.offset		= 0,
		.size		= 4 * NAND_BLOCK_SIZE,
		.mask_flags	= MTD_WRITEABLE,	/* force read-only */
	},
	{
		.name		= "U-Boot",
		.offset		= MTDPART_OFS_APPEND,	/* Offset = 0x80000 */
		.size		= 15 * NAND_BLOCK_SIZE,
		.mask_flags	= MTD_WRITEABLE,	/* force read-only */
	},
	{
		.name		= "U-Boot Env",
		.offset		= MTDPART_OFS_APPEND,	/* Offset = 0x260000 */
		.size		= 1 * NAND_BLOCK_SIZE,
	},
	{
		.name		= "Kernel",
		.offset		= MTDPART_OFS_APPEND,	/* Offset = 0x280000 */
		.size		= 32 * NAND_BLOCK_SIZE,
	},
	{
		.name		= "File System",
		.offset		= MTDPART_OFS_APPEND,	/* Offset = 0x680000 */
		.size		= MTDPART_SIZ_FULL,
	},
};

static struct omap_nand_platform_data omap3oswald_nand_data = {
	//temp set to 8 (yes 0 is 8-bit wide), change back to 16 for next revision
	.options	= 0,
	.parts		= omap3oswald_nand_partitions,
	.nr_parts	= ARRAY_SIZE(omap3oswald_nand_partitions),
	.dma_channel	= -1,		/* disable DMA in OMAP NAND driver */
	.nand_setup	= NULL,
	.dev_ready	= NULL,
};

static struct resource omap3oswald_nand_resource = {
	.flags		= IORESOURCE_MEM,
};

static struct platform_device omap3oswald_nand_device = {
	.name		= "omap2-nand",
	.id		= -1,
	.dev		= {
		.platform_data	= &omap3oswald_nand_data,
	},
	.num_resources	= 1,
	.resource	= &omap3oswald_nand_resource,
};

#include "sdram-micron-mt46h32m32lf-6.h"


static struct omap_uart_config omap3_oswald_uart_config __initdata = {
	.enabled_uarts	= ((1 << 0) | (1 << 1) | (1 << 2)),
};

////////////////////////////////////////////////////////////////////////////////
// MMC configuration...

static struct oswald_hsmmc_info mmc = 
	{
		.mmc		= 1,
		.wires		= 8,
		.gpio_wp	= MMC_WP_GPIO,
		.gpio_cd	= MMC_CD_GPIO,
	};



////////////////////////////////////////////////////////////////////////////////
// SPI related configuration...

static void ads7846_dev_init(void)
{
	if (gpio_request(TS_PEN_GPIO, "tsc2046 pendown") < 0) {
		printk(KERN_ERR "can't get tsc2046 pen down GPIO\n");
		return;
	}

	gpio_direction_input(TS_PEN_GPIO);

	omap_set_gpio_debounce(TS_PEN_GPIO, 1);
	omap_set_gpio_debounce_time(TS_PEN_GPIO, 0xa);
}

static int ads7846_get_pendown_state(void)
{
	//printk("GAHHAAA Not the PEN again!!!11!\n");
	return !gpio_get_value(TS_PEN_GPIO);
}

static struct ads7846_platform_data tsc2046_config __initdata = {
	.get_pendown_state		= ads7846_get_pendown_state,
	.keep_vref_on			= 1,
	.x_max                  = 0x0fff,
	.y_max                  = 0x0fff,
//	.y_plate_ohms           = 350,
	.x_plate_ohms           = 590,
	.pressure_max           = 255,
	.debounce_max           = 10,
	.debounce_tol           = 3,
	.debounce_rep           = 1,
//	.vaux_control		= ads7846_vaux_control,
	// probably need to set up more stuff about the screen here...
};


//FIXME: I don't like that we call "omap2" commands...
static struct omap2_mcspi_device_config tsc2046_mcspi_config = {
	.turbo_mode	= 0,
	.single_channel	= 1,  /* 0: slave, 1: master */
};

static struct omap2_mcspi_device_config aic3x_mcspi_config = {
	.turbo_mode	= 0,
	.single_channel	= 1,  /* 0: slave, 1: master */
};



static struct spi_board_info tsc2046_spi_board_info[] __initdata = {
	{
		
		 // TSC2046 operates at a max freqency of 2MHz, so
		 // operate slightly below at 1.5MHz

		.modalias			= "ads7846",
		.bus_num			= 4,
		.chip_select		= 0,
		.max_speed_hz		= 1500000,
		.controller_data	= &tsc2046_mcspi_config,
		.irq				= 0,
		.platform_data		= &tsc2046_config,
	},
};
static struct spi_board_info aic3x_spi_board_info[] __initdata = {
	{
		 
		.modalias			= "aic3x SPI codec",
		.bus_num			= 2,
		.chip_select		= 0,
		.max_speed_hz		= 500000,
		.controller_data	= &aic3x_mcspi_config,
		.irq				= 0,
//		.platform_data		= &tlv3x_config,
	},
};


static int __init omap3_oswald_spi_init(void) {
	tsc2046_spi_board_info[0].irq = gpio_to_irq(TS_PEN_GPIO);
	
	spi_register_board_info(tsc2046_spi_board_info, ARRAY_SIZE(tsc2046_spi_board_info));
	
	spi_register_board_info(aic3x_spi_board_info, ARRAY_SIZE(aic3x_spi_board_info));
	
	ads7846_dev_init();

	return 0;
}

////////////////////////////////////////////////////////////////////////////////
// I2C configuration...


static struct i2c_board_info __initdata oswald_i2c_line1[] = {
	{
		I2C_BOARD_INFO("bq27200", 0x55), //50kHz
		
		//FIXME: sould make new driver for our chips
		.type = "bq27200",
		.addr = 0x55,
	},
};

static struct i2c_board_info __initdata oswald_i2c_line2[] = {
	{
		I2C_BOARD_INFO("m41t65", 0x68), //100kHz
		.type = "m41t65",
		.addr = 0x68,
		.irq = INT_34XX_SYS_NIRQ,
	},
};

static struct i2c_board_info __initdata oswald_i2c_line3[] = {
	{
		I2C_BOARD_INFO("externalDVIMonitor", 0x00), //unknown address
	},
};


static int __init omap3_oswald_i2c_init(void) {

	//register our i2c devices
	omap_register_i2c_bus(1, 50,  oswald_i2c_line1, ARRAY_SIZE(oswald_i2c_line1));

	omap_register_i2c_bus(2, 100, oswald_i2c_line2, ARRAY_SIZE(oswald_i2c_line2));
	//platform_device_register(&oswald_rtc_device);

	//omap_register_i2c_bus(3, 100, oswald_i2c_line3, ARRAY_SIZE(oswald_i2c_line3));

	return 0;
}

////////////////////////////////////////////////////////////////////////////////

static void __init omap3_oswald_init_irq(void) {

	omap2_init_common_hw(mt46h32m32lf6_sdrc_params);
	omap_init_irq();
	omap_gpio_init();
}

////////////////////////////////////////////////////////////////////////////////
// LCD configuration...
/*
static struct platform_device omap3_oswald_lcd_device = {
	.name		= "omap3beagle_lcd", //temp fix later
	.id		= -1,
};

static struct omap_lcd_config omap3_oswald_lcd_config __initdata = {
	.ctrl_name	= "internal",
};
*/

////////////////////////////////////////////////////////////////////////////////
// LED configuration...

static struct gpio_led gpio_leds[] = {
	{
		.name			= "oswaldboard::usr",
		.default_trigger	= "heartbeat",
		.gpio			= USER_LED_GPIO,
	},
	{
		.name			= "oswaldboard::mmc",
		.default_trigger	= "mmc0",
		.gpio			= MMC_LED_GPIO,
	},
};

static struct gpio_led_platform_data gpio_led_info = {
	.leds		= gpio_leds,
	.num_leds	= ARRAY_SIZE(gpio_leds),
};

static struct platform_device leds_gpio = {
	.name	= "leds-gpio",
	.id		= -1,
	.dev	= {
		.platform_data	= &gpio_led_info,
	},
};

////////////////////////////////////////////////////////////////////////////////
// Power switch configuration...

static struct gpio_keys_button gpio_buttons[] = {
	{
		.code			= KEY_POWER,
		.type			= EV_PWR,
		.active_low		= 1,
		.gpio			= POWER_SW_GPIO,
		.desc			= "Power switch",
		.wakeup			= 1,
	},
};

static struct gpio_keys_platform_data gpio_key_info = {
	.buttons	= gpio_buttons,
	.nbuttons	= ARRAY_SIZE(gpio_buttons),
	.rep		= 1,
};

static struct platform_device keys_gpio = {
	.name	= "gpio-keys",
	.id	= -1,
	.dev	= {
		.platform_data	= &gpio_key_info,
	},
};



////////////////////////////////////////////////////////////////////////////////
// USB configuration...

static struct omap_usb_config omap3_oswald_usb_config __initdata = {
	.pins[0]	= 6,
	.pins[1]	= 6,
	.pins[2]	= 6,
	.rwc		= 0,
	.hmc_mode	= 0x11,					//FIXME ????WTF??
	.otg		= 0,
	.register_host = 1,
	.register_dev = 0,
};

static struct resource ohci_resources[] = {
	[0] = {
		.start   = OMAP34XX_HSUSB_HOST_BASE + 0x400,
		.end     = OMAP34XX_HSUSB_HOST_BASE + 0x400 + SZ_1K - 1,
		.flags   = IORESOURCE_MEM,
	},
	[1] = {         /* general IRQ */
		.start   = INT_34XX_OHCI_IRQ,
		.flags   = IORESOURCE_IRQ,
	}
};

static u64 ohci_dmamask = ~(u32)0;
static struct platform_device ohci_device = {
	.name           = "ohci-omap3",
	.id             = 0,
	.dev = {
		.dma_mask               = &ohci_dmamask,
		.coherent_dma_mask      = 0xffffffff,
		.platform_data          = &omap3_oswald_usb_config,
	},
	.num_resources  = ARRAY_SIZE(ohci_resources),
	.resource       = ohci_resources,
};


void __init usb_ohci_init(void)
{
	printk(KERN_ERR "Attempting to register FS-USB (OHCI) device\n");
	if (platform_device_register(&ohci_device) < 0) {
		printk(KERN_ERR "Unable to register FS-USB (OHCI) device\n");
		return;
	}
}

////////////////////////////////////////////////////////////////////////////////
/* DSS */

static int oswald_enable_dvi(struct omap_display *display)
{
	if (display->hw_config.panel_reset_gpio != -1)
		gpio_direction_output(display->hw_config.panel_reset_gpio, 1);

	return 0;
}

static void oswald_disable_dvi(struct omap_display *display)
{
	if (display->hw_config.panel_reset_gpio != -1)
		gpio_direction_output(display->hw_config.panel_reset_gpio, 0);
}

static struct omap_display_data oswald_display_data_dvi = {
	.type = OMAP_DISPLAY_TYPE_DPI,
	.name = "dvi",
	.panel_name = "panel-generic",
	.u.dpi.data_lines = 24,
	.panel_reset_gpio = DVI_EN_GPIO,
	.panel_enable = oswald_enable_dvi,
	.panel_disable = oswald_disable_dvi,
};


static int oswald_panel_enable_tv(struct omap_display *display)
{

	return 0;
}

static void oswald_panel_disable_tv(struct omap_display *display)
{

}

static struct omap_display_data oswald_display_data_tv = {
	.type = OMAP_DISPLAY_TYPE_VENC,
	.name = "tv",
	.u.venc.type = OMAP_DSS_VENC_TYPE_SVIDEO,
	.panel_enable = oswald_panel_enable_tv,
	.panel_disable = oswald_panel_disable_tv,
};

static struct omap_dss_platform_data oswald_dss_data = {
	.num_displays = 2,
	.displays = {
		&oswald_display_data_dvi,
		&oswald_display_data_tv,
	}
};

static struct platform_device oswald_dss_device = {
	.name          = "omap-dss",
	.id            = -1,
	.dev            = {
		.platform_data = &oswald_dss_data,
	},
};

static void __init oswald_display_init(void)
{
	int r;

	r = gpio_request(oswald_display_data_dvi.panel_reset_gpio, "DVI reset");
	if (r < 0)
		printk(KERN_ERR "Unable to get DVI reset GPIO\n");
}

////////////////////////////////////////////////////////////////////////////////





static struct omap_board_config_kernel omap3_oswald_config[] __initdata = {
	{ OMAP_TAG_UART,	&omap3_oswald_uart_config },
//	{ OMAP_TAG_LCD,		&omap3_oswald_lcd_config },
	//{ OMAP_TAG_USB,		&omap3_oswald_usb_config }, //temp disable to ensure standard boot (make sure this is the hang point)
};

static struct platform_device *omap3_oswald_devices[] __initdata = {
//	&omap3_oswald_lcd_device,
	&oswald_dss_device,
	&leds_gpio,
	&keys_gpio,
};


static void __init omap3oswald_flash_init(void)
{
	u8 cs = 0;
	u8 nandcs = GPMC_CS_NUM + 1;

	u32 gpmc_base_add = OMAP34XX_GPMC_VIRT;

	/* find out the chip-select on which NAND exists */
	while (cs < GPMC_CS_NUM) {
		u32 ret = 0;
		ret = gpmc_cs_read_reg(cs, GPMC_CS_CONFIG1);

		if ((ret & 0xC00) == 0x800) {
			printk(KERN_INFO "Found NAND on CS%d\n", cs);
			if (nandcs > GPMC_CS_NUM)
				nandcs = cs;
		}
		cs++;
	}

	if (nandcs > GPMC_CS_NUM) {
		printk(KERN_INFO "NAND: Unable to find configuration "
				 "in GPMC\n ");
		return;
	}

	if (nandcs < GPMC_CS_NUM) {
		omap3oswald_nand_data.cs = nandcs;
		omap3oswald_nand_data.gpmc_cs_baseaddr = (void *)
			(gpmc_base_add + GPMC_CS0_BASE + nandcs * GPMC_CS_SIZE);
		omap3oswald_nand_data.gpmc_baseaddr = (void *) (gpmc_base_add);

		printk(KERN_INFO "Registering NAND on CS%d\n", nandcs);
		if (platform_device_register(&omap3oswald_nand_device) < 0)
			printk(KERN_ERR "Unable to register NAND device\n");
		printk("NAND Registered.\n");
	}
}

static void __init omap3_oswald_init(void)
{
	gpio_request(V3_EN_GPIO, "3.3_EN");
	gpio_direction_output(V3_EN_GPIO, true);
	
	gpio_request(CODEC_RST_GPIO, "Codec_RST");
	gpio_direction_output(CODEC_RST_GPIO, false);

	gpio_request(BKLIGHT_EN_GPIO, "BKLIGHT_EN");
	gpio_direction_output(BKLIGHT_EN_GPIO, false);		//TODO: Change this to true for the final release


	platform_add_devices(omap3_oswald_devices,ARRAY_SIZE(omap3_oswald_devices));
	omap_board_config = omap3_oswald_config;
	omap_board_config_size = ARRAY_SIZE(omap3_oswald_config);
	omap_serial_init();
	omap3_oswald_i2c_init();
	omap3_oswald_spi_init();
	
	printk("Base board config done.\n");
	
	gpio_request(CODEC_RST_GPIO, "Codec_RST");
	gpio_direction_output(CODEC_RST_GPIO, true);
	
	usb_ohci_init();
	

	omap3oswald_flash_init();
	
	//bring up the SD card slot
	oswald_mmc_init(&mmc);
	oswald_display_init();
}

static void __init omap3_oswald_map_io(void)
{
	omap2_set_globals_343x();
	omap2_set_sdram_vram(1280 * 1024 * 4 * 3, 0);
	omap2_map_common_io();
}


MACHINE_START(OMAP3_BEAGLE, "OMAP3 OSWALD")
	/* Maintainer: Oregon State University - http://beaversouce.oregonstate.edu/cspfl */
	.phys_io	= 0x48000000,
	.io_pg_offst	= ((0xd8000000) >> 18) & 0xfffc,
	.boot_params	= 0x80000100,
	.map_io		= omap3_oswald_map_io,
	.init_irq	= omap3_oswald_init_irq,
	.init_machine	= omap3_oswald_init,
	.timer		= &omap_timer,
MACHINE_END
