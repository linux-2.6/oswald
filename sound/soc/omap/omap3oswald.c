/*
 * omap3oswald.c  --  SoC audio for OMAP3 OSWALD
 *
 * Author: Kevin Kemper <kkemper@gmail.com>
 * 
 * Based on the omap3beagel driver by Steve Sakoman <steve@sakoman.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include <linux/clk.h>
#include <linux/platform_device.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>

#include <asm/mach-types.h>
#include <mach/hardware.h>
#include <mach/gpio.h>
#include <mach/mcbsp.h>

#include "omap-mcbsp.h"
#include "omap-pcm.h"
#include "../codecs/tlv320aic3x.h"

#define DEBUG_BEN  //printk("<('')>:%s: %d\n", __FILE__, __LINE__);

static int oswald_spk_func;
static int oswald_jack_func;
static int oswald_mic_func;

static void oswald_ext_control(struct snd_soc_codec *codec)
{
	if (oswald_spk_func)
		snd_soc_dapm_enable_pin(codec, "Speaker");
	else
		snd_soc_dapm_disable_pin(codec, "Speaker");

	if (oswald_jack_func)
		snd_soc_dapm_enable_pin(codec, "Headphone Jack");
	else
		snd_soc_dapm_disable_pin(codec, "Headphone Jack");

	if (oswald_mic_func)
		snd_soc_dapm_enable_pin(codec, "Mic");
	else
		snd_soc_dapm_disable_pin(codec, "Mic");

	snd_soc_dapm_sync(codec);
}

static int oswald_startup(struct snd_pcm_substream *substream) {

	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_codec *codec = rtd->socdev->codec;

	oswald_ext_control(codec);
	
	return 0;
}

static int oswald_hw_params(struct snd_pcm_substream *substream,
	struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->dai->codec_dai;
	struct snd_soc_dai *cpu_dai = rtd->dai->cpu_dai;
	int ret;
	u8 clkDivide;
	
	/* Set codec DAI configuration */
	ret = snd_soc_dai_set_fmt(codec_dai,
							SND_SOC_DAIFMT_I2S |
							SND_SOC_DAIFMT_NB_NF |
							SND_SOC_DAIFMT_CBS_CFS);	//set to slave mode
//							SND_SOC_DAIFMT_CBM_CFM);

	if (ret < 0) {
		printk(KERN_ERR "can't set codec DAI configuration\n");
		return ret;
	}

	/* Set cpu DAI configuration */
	ret = snd_soc_dai_set_fmt(cpu_dai,
							SND_SOC_DAIFMT_I2S |
							SND_SOC_DAIFMT_NB_NF |
							SND_SOC_DAIFMT_CBS_CFS);	//set to master mode
//							SND_SOC_DAIFMT_CBM_CFM);
	if (ret < 0) {
		printk(KERN_ERR "can't set cpu DAI configuration\n");
		return ret;
	}


	/* bit rate */
	//Assuming FCLK is at 96MHz...
	
	//For 16bit audio only...
	switch (params_rate(params)) {
	case 12000:
		clkDivide = 250;	//BCLK = 0.384 MHz
		break;
	case 16000:
		clkDivide = 187;	//BCLK = 0.513 MHz
		break;
	case 22050:
		clkDivide = 136;	//BCLK = 0.705 MHz
		break;
	case 24000:
		clkDivide = 125;	//BCLK = 0.768 MHz
		break;
	case 32000:
		clkDivide = 94;		//BCLK = 1.021 MHz
		break;
	case 44100:
		clkDivide = 68;		//BCLK = 1.411 MHz
		break;
	case 48000:
		clkDivide = 63;		//BCLK = 1.523 MHz
		break;
	default:
		printk(KERN_ERR "OSWALD audio hw params: unknown rate %d\n",
			params_rate(params));
		return -EINVAL;
	}
	snd_soc_dai_set_clkdiv(cpu_dai, OMAP_MCBSP_CLKGDV, clkDivide);


	/* Set the codec system clock for DAC and ADC */
	ret = snd_soc_dai_set_sysclk(codec_dai, 0, 12000000,
				     SND_SOC_CLOCK_IN);
	if (ret < 0) {
		printk(KERN_ERR "can't set codec system clock\n");
		return ret;
	}
	DEBUG_BEN
	return 0;
}


static struct snd_soc_ops omap3oswald_ops = {
	.startup = oswald_startup,
	.hw_params = oswald_hw_params,
};


static int oswald_get_spk(struct snd_kcontrol *kcontrol,
			struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = oswald_spk_func;

	return 0;
}

static int oswald_set_spk(struct snd_kcontrol *kcontrol,
			struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec =  snd_kcontrol_chip(kcontrol);

	if (oswald_spk_func == ucontrol->value.integer.value[0])
		return 0;

	oswald_spk_func = ucontrol->value.integer.value[0];
	oswald_ext_control(codec);

	return 1;
}

static int oswald_get_jack(struct snd_kcontrol *kcontrol,
			 struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = oswald_jack_func;

	return 0;
}

static int oswald_set_jack(struct snd_kcontrol *kcontrol,
			 struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec =  snd_kcontrol_chip(kcontrol);

	if (oswald_jack_func == ucontrol->value.integer.value[0])
		return 0;

	oswald_jack_func = ucontrol->value.integer.value[0];
	oswald_ext_control(codec);

	return 1;
}

static int oswald_get_input(struct snd_kcontrol *kcontrol,
			  struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = oswald_mic_func;

	return 0;
}

static int oswald_set_input(struct snd_kcontrol *kcontrol,
			  struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec =  snd_kcontrol_chip(kcontrol);

	if (oswald_mic_func == ucontrol->value.integer.value[0])
		return 0;

	oswald_mic_func = ucontrol->value.integer.value[0];
	oswald_ext_control(codec);

	return 1;
}


static const struct snd_soc_dapm_widget aic33_dapm_widgets[] = {
	SND_SOC_DAPM_HP("Headphone Jack", NULL),
	SND_SOC_DAPM_MIC("Mic", NULL),
	SND_SOC_DAPM_SPK("Speaker", NULL),
};

static const struct snd_soc_dapm_route audio_map[] = {
	/* Headphone connected to HPLOUT, HPROUT */
	{"Headphone Jack", NULL, "HPLOUT"},
	{"Headphone Jack", NULL, "HPROUT"},

	{"Speaker", NULL, "HPLCOM"},
	{"Speaker", NULL, "HPRCOM"},
//	{"Speaker", "single-ended", "Left DAC_L1 Mixer"},
//	{"Speaker", "differential of HPLCOM", "Right DAC_R1 Mixer"},


/* Mic connected to (MIC3L | MIC3R) */
//	{"MIC3L", NULL, "Mic Bias 2V"},
	{"MIC3R", NULL, "Mic Bias 2V"},
	{"Mic Bias 2V", NULL, "Mic"},
};

static const char *spk_function[] = {"Off", "On"};
static const char *hp_function[] = {"Off", "On"};
static const char *mic_function[] = {"Off", "On"};
static const struct soc_enum oswald_enum[] = {
	SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(spk_function), spk_function),
	SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(hp_function), hp_function),
	SOC_ENUM_SINGLE_EXT(ARRAY_SIZE(mic_function), mic_function),
};

static const struct snd_kcontrol_new aic33_oswald_controls[] = {
	SOC_ENUM_EXT("Speaker Function", oswald_enum[0], oswald_get_spk, oswald_set_spk),
	SOC_ENUM_EXT("Headphone Function", oswald_enum[1], oswald_get_jack, oswald_set_jack),
	SOC_ENUM_EXT("Mic Function",  oswald_enum[2], oswald_get_input, oswald_set_input),
};

static int omap3oswald_aic33_init(struct snd_soc_codec *codec) {

	int i, err;
	
	/* Not connected */
	snd_soc_dapm_nc_pin(codec, "MONO_LOUT");
	snd_soc_dapm_nc_pin(codec, "RLOUT");
	snd_soc_dapm_nc_pin(codec, "LLOUT");
	snd_soc_dapm_nc_pin(codec, "LINE1L");
	snd_soc_dapm_nc_pin(codec, "LINE1R");
	snd_soc_dapm_nc_pin(codec, "LINE2L");
	snd_soc_dapm_nc_pin(codec, "LINE2R");
	snd_soc_dapm_nc_pin(codec, "MIC3L");


	/* always connected */
	snd_soc_dapm_enable_pin(codec, "Headphone Jack");
	snd_soc_dapm_enable_pin(codec, "Speaker");
	snd_soc_dapm_enable_pin(codec, "Mic");

	/* Add OSWALD specific controls */
	for (i = 0; i < ARRAY_SIZE(aic33_oswald_controls); i++) {
		err = snd_ctl_add(codec->card,
			snd_soc_cnew(&aic33_oswald_controls[i], codec, NULL));
		if (err < 0)
			return err;
	}

	/* Add OSWALD specific widgets */
	snd_soc_dapm_new_controls(codec, aic33_dapm_widgets,
				  ARRAY_SIZE(aic33_dapm_widgets));

	/* Set up OSWALD specific audio path audio_map */
	snd_soc_dapm_add_routes(codec, audio_map, ARRAY_SIZE(audio_map));


	snd_soc_dapm_sync(codec);
	
	DEBUG_BEN

	return 0;
}

//XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX 

/* Digital audio interface glue - connects codec <--> CPU */
static struct snd_soc_dai_link omap3oswald_dai = {
	.name = "tlv320aic3x",
	.stream_name = "AIC33",
	.cpu_dai = &omap_mcbsp_dai[0],					//XXX: I don't think this is correct... but all of the others use it...
	.codec_dai = &aic3x_dai,
	.init = omap3oswald_aic33_init,
	.ops = &omap3oswald_ops,
};

/* Audio machine driver */
static struct snd_soc_machine snd_soc_machine_omap3oswald = {
	.name = "omap3oswald",
	.dai_link = &omap3oswald_dai,
	.num_links = 1,
};

/* Audio private data */
static struct aic3x_setup_data omap3oswald_aic33_setup = {
//	.i2c_bus = 2,
//	.i2c_address = 0x18,
	.spi = 2,
	.gpio_func[0] = AIC3X_GPIO1_FUNC_DISABLED,
	.gpio_func[1] = AIC3X_GPIO2_FUNC_DISABLED,
};

/* Audio subsystem */
static struct snd_soc_device omap3oswald_snd_devdata = {
	.machine = &snd_soc_machine_omap3oswald,
	.platform = &omap_soc_platform,
	.codec_dev = &soc_codec_dev_aic3x,
	.codec_data = &omap3oswald_aic33_setup,
};


//XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX XXX


static struct platform_device *omap3oswald_snd_device;



static int __init omap3oswald_soc_init(void)
{
	int err;
//	struct device *dev;
	
//	if (!machine_is_omap3_beagle()) {					//FIXME
//		pr_debug("Not OMAP3 Beagle!\n");
//		return -ENODEV;
//	}
	pr_info("OMAP3 OSWALD SoC init\n");

	DEBUG_BEN

	omap3oswald_snd_device = platform_device_alloc("soc-audio", -1);
	if (!omap3oswald_snd_device) {
		printk(KERN_ERR "Platform device allocation failed\n");
		return -ENOMEM;
	}
		
	DEBUG_BEN

	platform_set_drvdata(omap3oswald_snd_device, &omap3oswald_snd_devdata);
	omap3oswald_snd_devdata.dev = &omap3oswald_snd_device->dev;
	*(unsigned int *)omap3oswald_dai.cpu_dai->private_data = 1; /* McBSP2 */

	DEBUG_BEN

	err = platform_device_add(omap3oswald_snd_device);
	if (err)
		goto err1;


	return 0;

err1:
	printk(KERN_ERR "ERR1, Unable to add platform device\n");
	platform_device_put(omap3oswald_snd_device);

	return err;
}

static void __exit omap3oswald_soc_exit(void)
{
	platform_device_unregister(omap3oswald_snd_device);
}

module_init(omap3oswald_soc_init);
module_exit(omap3oswald_soc_exit);

MODULE_AUTHOR("Kevin Kemper <kkemper@gmail.com>");
MODULE_DESCRIPTION("ALSA SoC OMAP3 OSWALD");
MODULE_LICENSE("GPL");
